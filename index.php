<?php
require_once("config.php");

$sql = new Sql();

$filename = "categorias.csv";

$cache = [];

if(file_exists($filename)){

  $file = fopen($filename, "r");

  $categories = [];

  while($row = fgets($file)){

      array_push($categories, explode(",",$row)) ;
  }

  foreach($categories as $category){

    $arrInitials = explode("-", $category[0]);

    $name = utf8_encode($category[1]);

    /**
     * $arrInitials = grupo de siglas
     * $initials = sigla única
     */
    foreach ($arrInitials as $initials){

      if(count($cache) > 0){

        // Loop no cache (key = sigla, value = idPai)         
        foreach ($cache as $key => $value){         
          
          $idPai = null;

          if($key == $initials) $idPai = $key;
          else continue;

          $sql->query('INSERT INTO srvc_tbl_categoria (pai, nome, sigla)
          VALUES (:pai, :nome, :sigla)',[
          ':pai'=>$idPai,
          ':nome'=>$name,
          ':sigla'=>$key,
          ]);
        }        
      }else{
        $sql->query('INSERT INTO srvc_tbl_categoria (pai, nome, sigla)
          VALUES (:pai, :nome, :sigla)',[
          ':pai'=>null,
          ':nome'=>$name,
          ':sigla'=>$initials,
        ]);
      }

      $results = $sql->select('SELECT * FROM srvc_tbl_categoria');
      $data = $results[0];
      // Preenchimento da variável de cache
      array_push($cache, $data['sigla'], $data['id']);
    }
  }

  var_dump($cache); 
  fclose($file);
}

?>
